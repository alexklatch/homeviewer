package com.example.homeviewer.AdminWebsite;

public class JsonConst {

    public static int RTP_LINKS = 0;
    public static int SIP_CONTACTS = 1;
    public static int NET_INFO = 2;
    public static int DEVICES = 3;
    public static int WIDGETS = 4;
    public static int LOCATION = 5;

    public static String[] ARRAYS = new String[]{
            "rtp_Links",
            "sip_contacts",
            "net_info",
            "devices",
            "widgets",
            "conf_info"
    };

}
