package com.example.homeviewer.AdminWebsite;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;

import com.example.homeviewer.Utils.EventBus.Event;
import com.example.homeviewer.Utils.EventBus.EventBus;
import com.example.homeviewer.Utils.EventBus.Events;
import com.example.homeviewer.Utils.JsonUtils.JsonDataRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.example.homeviewer.AdminWebsite.JsonConst.ARRAYS;
import static com.example.homeviewer.Utils.JsonUtils.JsonSaver.create;
import static com.example.homeviewer.Utils.JsonUtils.JsonSaver.read;

public class JsonUpdaterService extends Service {

    private final String TAG = "JsonUpdater";
    private String current_json;
    private boolean hasConfig = false;

    private IBinder mBinder = new ServBinder();

    public String getData(int type) throws JSONException {
        if (hasConfig) {
            JSONObject jsonObject = new JSONObject(current_json);
            Log.d(TAG, "Trying to get type " + type);
            JSONArray data = jsonObject.getJSONArray(ARRAYS[type]);
            return data.toString();
        } else return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        JsonDataRequest jsonDataRequest = new JsonDataRequest(getApplicationContext());
        jsonDataRequest.reqConfig();
        Log.d(TAG, "Created!");

        current_json = read(getApplicationContext(), "config.json");
        if (current_json != null) {
            hasConfig = true;
        }

        Disposable timerTaskSubscription = Observable.interval(0, 30, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(aLong ->
                        {
                            jsonDataRequest.reqConfig();
                            EventBus.subscribe(msg -> {
                                Event event = (Event) msg;
                                int event_id = event.getEventId();
                                switch (event_id) {
                                    case Events.ID_CONFIG_LOADED:

                                        Log.d(TAG, "Loaded!");
                                        JSONObject new_json = jsonDataRequest.getDb();
                                        Log.d(TAG, current_json);
                                        Log.d(TAG, new_json.toString());

                                        if (!hasConfig) {
                                            create(getApplicationContext(), "config.json", new_json.toString());
                                        }

                                        if (!current_json.equals(new_json.toString())) {
                                            Log.d(TAG, "Update config!");
                                            current_json = new_json.toString();
                                        }
                                        break;
                                }
                            });
                        },
                        throwable -> {
                            Log.d(TAG, throwable.getMessage());
                        });

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG, "Service bound");
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        Log.v(TAG, "Service re-bound");
        super.onRebind(intent);
    }

    public boolean onUnbind(Intent intent) {
        Log.v(TAG, "Service unbind by " + intent.getClass().toString());
        return true;
    }

    public class ServBinder extends Binder {
        public JsonUpdaterService getService() {
            return JsonUpdaterService.this;
        }
    }

}
