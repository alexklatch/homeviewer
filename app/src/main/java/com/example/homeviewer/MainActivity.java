package com.example.homeviewer;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.homeviewer.AdminWebsite.JsonUpdaterService;
import com.example.homeviewer.Services.CallVisualService;
import com.example.homeviewer.Services.MediaService;
import com.example.homeviewer.Services.SipService;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private ViewPager main_slide;
    private JsonUpdaterService mJsonUpdateService;
    private boolean isJsonServiceBind = false;

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            isJsonServiceBind = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            JsonUpdaterService.ServBinder myBinder = (JsonUpdaterService.ServBinder) service;
            mJsonUpdateService = myBinder.getService();
            isJsonServiceBind = true;
        }
    };

    private void startService(Class service) {
        Intent intent = new Intent(getApplicationContext(), service);
        startService(intent);
    }

    private void startServiceBind(Class service) {
        Intent intent = new Intent(getApplicationContext(), service);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        startService(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_swipe_layout);

        startService(MediaService.class);
        startService(CallVisualService.class);
        startServiceBind(JsonUpdaterService.class);
        //startServiceBind(SipService.class);

        main_slide = findViewById(R.id.main_view_pager);
        main_slide.setAdapter(new SlideAdapter(getSupportFragmentManager()));
        main_slide.setCurrentItem(1);
        main_slide.setOffscreenPageLimit(1);

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(main_slide);

    }
}
