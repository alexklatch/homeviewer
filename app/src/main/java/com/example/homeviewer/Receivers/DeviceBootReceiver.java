package com.example.homeviewer.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DeviceBootReceiver extends BroadcastReceiver {

    private String TAG = "HomeViewer.OnBoot";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Device started");
    }
}
