package com.example.homeviewer.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.homeviewer.SipFiles.CallActivity;
import com.example.homeviewer.Utils.EventBus.Event;
import com.example.homeviewer.Utils.EventBus.EventBus;
import com.example.homeviewer.Utils.EventBus.Events;

public class CallVisualService extends Service {

    private static final String TAG = "HomeViewer.Visual";
    private Context context = this;

    private final int SHOW_CALL_PAGE = 0;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Visual service created");

        EventBus.subscribe(msg ->
                {
                    Event event = (Event) msg;
                    int event_id = event.getEventId();
                    switch (event_id) {
                        case Events.ID_INCOMING_CALL:
                            Log.d(TAG, "Event incoming call! " + event.getUsername());
                            Message message = handler.obtainMessage(SHOW_CALL_PAGE);
                            message.sendToTarget();
                            break;
                        case Events.ID_ANSWERED_CALL:
                            Log.d(TAG, "Evet answer call!");

                            break;
                    }
                }
        );
    }

    Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Log.d(TAG, "Handler");
            switch (msg.arg1) {
                case SHOW_CALL_PAGE:
                    Toast.makeText(getApplicationContext(), "Call in", Toast.LENGTH_LONG);
                    Intent mIntent = new Intent(context, CallActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(mIntent);
                    break;
            }
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
