package com.example.homeviewer.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.example.homeviewer.R;
import com.example.homeviewer.Utils.EventBus.Event;
import com.example.homeviewer.Utils.EventBus.EventBus;
import com.example.homeviewer.Utils.EventBus.Events;
import com.example.homeviewer.Utils.ParseConfig;
import com.example.homeviewer.Utils.Utils;

import java.io.File;

public class ConfigService extends Service {

    private Context appContext = getApplicationContext();

    @Override
    public void onCreate() {
        super.onCreate();

        String path = Utils.getPrefs(appContext, "FilePath");

        if (path == null) {
            ParseConfig parseConfig = new ParseConfig(R.raw.config, appContext);
        }

        File file = new File(Utils.getPrefs(appContext, "FilePath"));


        EventBus.subscribe(msg -> {
            Event event = (Event) msg;
            int event_id = event.getEventId();
            switch (event_id) {
                case Events.ID_GET_CONFIG:
                    break;
                case Events.ID_CHANGE_CONFIG:
                    break;
            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
