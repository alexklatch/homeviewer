package com.example.homeviewer.Services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.LongDef;
import androidx.annotation.Nullable;

import com.example.homeviewer.Utils.EventBus.Event;
import com.example.homeviewer.Utils.EventBus.EventBus;
import com.example.homeviewer.Utils.EventBus.Events;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MediaService extends Service {

    private final String TAG = "HomeViewer.MediaService";
    private int[] start_time = new int[]{12, 45};
    private String result = "0:00";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        startTimer();
        return null;
    }

    private void startTimer() {
        getObservable()
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getObserver());
    }

    private Observable<? extends Long> getObservable() {
        return Observable.timer(1, TimeUnit.MINUTES);
    }

    private Observer<Long> getObserver() {
        return new Observer<Long>() {

            @SuppressLint("LongLogTag")
            @Override
            public void onSubscribe(Disposable d) {
                Log.d(TAG, " onSubscribe : " + d.isDisposed());
            }

            @SuppressLint("LongLogTag")
            @Override
            public void onNext(Long value) {

                int h = start_time[0];
                int m = start_time[1];

                m++;

                if (m < 10) {
                    result = h + ":0" + m;
                } else {
                    result = h + ":" + m;
                }

                if (m >= 59) {
                    m = 0;
                    h++;
                }

                if (h > 24) {
                    h = 0;
                }

                Log.d(TAG, start_time[0] + " " + start_time[1]);

                start_time[0] = h;
                start_time[1] = m;

                EventBus.post(new Event(Events.ID_CURRNET_TIME, result));
                Log.d(TAG, result);
            }

            @SuppressLint("LongLogTag")
            @Override
            public void onError(Throwable e) {
                Log.d(TAG, " onError : " + e.getMessage());
            }

            @SuppressLint("LongLogTag")
            @Override
            public void onComplete() {
                Log.d(TAG, "Minute pass");
                startTimer();
            }
        };
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Service created");
        EventBus.post(new Event(Events.ID_CURRNET_TIME, result));
        startTimer();
    }
}