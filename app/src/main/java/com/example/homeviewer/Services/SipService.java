package com.example.homeviewer.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.homeviewer.SipFiles.SipAccount;
import com.example.homeviewer.SipFiles.SipLogWriter;

import org.pjsip.pjsua2.AccountConfig;
import org.pjsip.pjsua2.AuthCredInfo;
import org.pjsip.pjsua2.Endpoint;
import org.pjsip.pjsua2.EpConfig;
import org.pjsip.pjsua2.LogConfig;
import org.pjsip.pjsua2.MediaConfig;
import org.pjsip.pjsua2.TransportConfig;
import org.pjsip.pjsua2.UaConfig;
import org.pjsip.pjsua2.pj_log_decoration;
import org.pjsip.pjsua2.pjsip_transport_type_e;

import java.util.ArrayList;

import static org.pjsip.pjsua2.pjmedia_echo_flag.PJMEDIA_ECHO_AGGRESSIVENESS_AGGRESSIVE;
import static org.pjsip.pjsua2.pjmedia_echo_flag.PJMEDIA_ECHO_USE_NOISE_SUPPRESSOR;
import static org.pjsip.pjsua2.pjmedia_echo_flag.PJMEDIA_ECHO_WEBRTC;


public class SipService extends Service {

    public final static String TAG = "HomeViewer.SipService";

    private SipAccount acc;


    static {
        System.loadLibrary("pjsua2");
        Log.d(TAG, "PJSIP loaded");
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    private Endpoint endpoint;

    @Override
    public void onDestroy() {
        super.onDestroy();
        acc.delete();

        // Explicitly destroy and delete endpoint
        try {
            endpoint.libDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        endpoint.delete();

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        ArrayList<String> sipLink = intent.getStringArrayListExtra("options");
        String username = "user4";
        String password = "user4pw";
        String ip = "192.168.31.142";

        Log.d(TAG, username + " " + password + " " + ip);

        try {
            Log.d(TAG, "Trying to init pjsip");
            initSip();
        } catch (Exception e) {
            Log.d(TAG, "Initialisation failed");
            Log.d(TAG, e.toString());
        }
        Log.d(TAG, "SIP init");
        AccountConfig accountConfig = new AccountConfig();
        accountConfig.setIdUri("sip:" + username + "@" + ip);
        accountConfig.getRegConfig().setRegistrarUri("sip:" + ip);
        AuthCredInfo cred = new AuthCredInfo("tcp", "*", username, 0, password);
        accountConfig.getSipConfig().getAuthCreds().add(cred);

        acc = new SipAccount();

        try {
            acc.create(accountConfig);
            Log.d(TAG, String.valueOf(acc.isValid()));
            Log.d(TAG, String.valueOf(acc.getId()));
            Log.d(TAG, acc.getInfo().getUri());
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Account creation failed!\n" + e.toString());
        }
        return null;
    }

    private void initSip() throws Exception {
        endpoint = new Endpoint();
        endpoint.libCreate();

        EpConfig epConfig = new EpConfig();
        MediaConfig mediaConfig = epConfig.getMedConfig();

        mediaConfig.setEcOptions(PJMEDIA_ECHO_WEBRTC | PJMEDIA_ECHO_USE_NOISE_SUPPRESSOR | PJMEDIA_ECHO_AGGRESSIVENESS_AGGRESSIVE);
        mediaConfig.setEcTailLen(10);

        epConfig.getLogConfig().setLevel(5);
        epConfig.getLogConfig().setConsoleLevel(5);

        Log.d(TAG, "Log config");

        LogConfig logConfig = epConfig.getLogConfig();
        logConfig.setWriter(new SipLogWriter());
        logConfig.setDecor(logConfig.getDecor() &
                ~(pj_log_decoration.PJ_LOG_HAS_CR |
                        pj_log_decoration.PJ_LOG_HAS_NEWLINE));

        Log.d(TAG, "Final ep init");

        UaConfig ua_cfg = epConfig.getUaConfig();
        ua_cfg.setUserAgent("Pjsua2 Android " + endpoint.libVersion().getFull());

        /* STUN server. */
        //StringVector stun_servers = new StringVector();
        //stun_servers.add("stun.pjsip.org");
        //ua_cfg.setStunServer(stun_servers);

        /* No worker thread */
        ua_cfg.setThreadCnt(0);
        ua_cfg.setMainThreadOnly(true);


        //endpoint.libInit(epConfig);

        try {
            endpoint.libInit(epConfig);
        } catch (Exception e) {
            Log.d(TAG, e.toString());
            return;
        }


        Log.d(TAG, "transport config");

        TransportConfig transportConfig = new TransportConfig();
        transportConfig.setPort(5060);

        Log.d(TAG, "Start");

        endpoint.transportCreate(pjsip_transport_type_e.PJSIP_TRANSPORT_UDP, transportConfig);
        endpoint.libStart();
    }

}
