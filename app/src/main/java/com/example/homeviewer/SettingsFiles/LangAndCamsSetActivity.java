package com.example.homeviewer.SettingsFiles;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.homeviewer.R;

public class LangAndCamsSetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cameras_settings_layout);
        ImageButton back_btn = findViewById(R.id.back_btn_2);
        back_btn.setOnClickListener(v1 -> onBackPressed());
    }
}
