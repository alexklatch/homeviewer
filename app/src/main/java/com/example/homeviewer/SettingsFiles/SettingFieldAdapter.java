package com.example.homeviewer.SettingsFiles;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.homeviewer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class SettingFieldAdapter extends RecyclerView.Adapter<SettingFieldAdapter.SettingHolderView> {

    private Context context;

    private JSONArray d_jsons;


    private final String TAG = "HomeViewer.SetFieldAdapter";

    public SettingFieldAdapter(Context context, JSONArray d_jsons) {
        this.context = context;
        this.d_jsons = d_jsons;
    }

    @NonNull
    @Override
    public SettingHolderView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.info_item, parent, false);
        return new SettingHolderView(view);
    }

    @SuppressLint("LongLogTag")
    private HashMap<String, String> parseObject(JSONObject cj_data) throws JSONException {
        HashMap<String, String> objs = new HashMap<>();
        for (Iterator<String> it = cj_data.keys(); it.hasNext(); ) {
            String key = it.next();
            String val = String.valueOf(cj_data.get(key));
            objs.put(key, val);
            Log.d(TAG, key + " " + val);
        }
        return objs;
    }


    @SuppressLint("LongLogTag")
    private void parseArray(JSONArray cj_data) throws JSONException {
        for (int i = 0; i < cj_data.length(); i++) {
            parseObject(cj_data.getJSONObject(i));
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onBindViewHolder(@NonNull SettingHolderView holder, int position) {
        JSONArray cj_data = null;
        try {
            cj_data = d_jsons.getJSONArray(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, cj_data.toString());

        List<String> keys = new ArrayList<>();
        List<String> vals = new ArrayList<>();

        try {
            parseArray(cj_data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.title.setText("test");

        String[] k = new String[keys.size()];
        String[] v = new String[vals.size()];

        keys.toArray(k);
        vals.toArray(v);

        SettingListAdapter settingListAdapter = new SettingListAdapter(context, k, v);
        holder.info_list.setAdapter(settingListAdapter);

    }

    @Override
    public int getItemCount() {
        return d_jsons.length();
    }

    public class SettingHolderView extends RecyclerView.ViewHolder {

        ListView info_list;
        TextView title;

        public SettingHolderView(@NonNull View itemView) {
            super(itemView);
            info_list = itemView.findViewById(R.id.info_list);
            title = itemView.findViewById(R.id.titile_text);
        }
    }

}
