package com.example.homeviewer.SettingsFiles;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.example.homeviewer.AdminWebsite.JsonUpdaterService;
import com.example.homeviewer.R;
import com.example.homeviewer.Utils.EventBus.Event;
import com.example.homeviewer.Utils.EventBus.EventBus;

import org.jetbrains.annotations.NotNull;

import static com.example.homeviewer.AdminWebsite.JsonConst.DEVICES;
import static com.example.homeviewer.AdminWebsite.JsonConst.NET_INFO;
import static com.example.homeviewer.AdminWebsite.JsonConst.RTP_LINKS;
import static com.example.homeviewer.AdminWebsite.JsonConst.SIP_CONTACTS;
import static com.example.homeviewer.SettingsFiles.SettingsTypes.ADD;
import static com.example.homeviewer.SettingsFiles.SettingsTypes.ADDRESSING;
import static com.example.homeviewer.SettingsFiles.SettingsTypes.DEVS;
import static com.example.homeviewer.SettingsFiles.SettingsTypes.INFORMATION;
import static com.example.homeviewer.SettingsFiles.SettingsTypes.MAIN;
import static com.example.homeviewer.SettingsFiles.SettingsTypes.NETWORK;
import static com.example.homeviewer.Utils.EventBus.Events.ID_SETTING_CLICK;

public class SettingFragment extends Fragment {

    @NotNull
    public static SettingFragment newInstance() {
        SettingFragment f = new SettingFragment();
        return f;
    }

    private JsonUpdaterService mJsonUpdateService;
    private boolean isJsonServiceBind = false;

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            isJsonServiceBind = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            JsonUpdaterService.ServBinder myBinder = (JsonUpdaterService.ServBinder) service;
            mJsonUpdateService = myBinder.getService();
            isJsonServiceBind = true;
        }
    };

    private final String TAG = "HomeViewer.Settings";

    private final int[] btns = new int[]{
            R.id.info_btn,
            R.id.device_btn,
            R.id.adr_btn,
            R.id.net_btn,
            R.id.main_btn,
            R.id.addons_btn
    };

    private void startIntent(Class d, String data) {
        Intent intent = new Intent(getContext(), d);
        intent.putExtra("data", data);
        startActivity(intent);
    }

    private void bindService(Class service) {
        Intent intent = new Intent(getContext(), service);
        getActivity().bindService(intent, mServiceConnection, Context.BIND_ABOVE_CLIENT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.activity_settings, null);

        bindService(JsonUpdaterService.class);

        for (int id : btns) {
            Button b = v.findViewById(id);
            b.setTextColor(Color.WHITE);
            b.setOnClickListener(v1 -> EventBus.post(new Event(ID_SETTING_CLICK, v1.getId())));
        }

        EventBus.subscribe(msg -> {
            Event event = (Event) msg;
            int event_id = event.getEventId();
            int b_id = event.getVal();
            if (event_id == ID_SETTING_CLICK) {
                if (isJsonServiceBind) {
                    for (int i = 0; i < btns.length; i++) {
                        if (b_id == btns[i]) {
                            switch (i) {
                                case INFORMATION:
                                    startIntent(SettingScreen.class, mJsonUpdateService.getData(RTP_LINKS));
                                    break;
                                case DEVS:
                                    startIntent(SettingScreen.class, mJsonUpdateService.getData(DEVICES));
                                    break;
                                case ADDRESSING:
                                    startIntent(SettingScreen.class, mJsonUpdateService.getData(SIP_CONTACTS));
                                    break;
                                case NETWORK:
                                    startIntent(SettingScreen.class, mJsonUpdateService.getData(NET_INFO));
                                    break;
                                case MAIN:
                                    startIntent(SettingScreen.class, "data");
                                    break;
                                case ADD:
                                    startIntent(LangAndCamsSetActivity.class, "data");
                                    break;
                            }
                            if (i == 5) {
                            } else {

                            }
                        }
                    }
                } else {
                    Log.d(TAG, "Json service is not connected!");
                }
            }
        });

        return v;
    }

}
