package com.example.homeviewer.SettingsFiles;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.homeviewer.R;

public class SettingListAdapter extends ArrayAdapter<String> {

    private final Context context;
    private String[] keys;
    private String[] vals;

    private final String TAG = "HomeViewer.SetListAdapter";

    public SettingListAdapter(Context context, String[] keys, String[] vals) {
        super(context, R.layout.setting_list, keys);
        this.context = context;
        this.keys = keys;
        this.vals = vals;
    }

    @SuppressLint("LongLogTag")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View set_view = inflater.inflate(R.layout.setting_list, null, true);

        TextView value_txt = set_view.findViewById(R.id.value_txt);
        TextView key_txt = set_view.findViewById(R.id.key_txt);

        Log.d(TAG, vals[position] + " " + keys[position]);

        value_txt.setText(vals[position]);
        key_txt.setText(keys[position]);

        return set_view;
    }

    @Override
    public int getCount() {
        return keys.length;
    }
}
