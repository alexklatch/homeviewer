package com.example.homeviewer.SettingsFiles;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.homeviewer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SettingScreen extends AppCompatActivity {

    private final String TAG = "HomeViewer.Setting";
    private List<JSONArray> d_jsons = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_info_layout);

        Bundle bundle = getIntent().getExtras();
        String data = bundle.getString("data");

        JSONArray jdata = null;

        try {
            jdata = new JSONArray(data);
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Unable to parse json!", Toast.LENGTH_LONG);
            Log.d(TAG, e.toString());
            onBackPressed();
        }

        Log.d(TAG, String.valueOf(jdata));

        RecyclerView recyclerView = findViewById(R.id.recycler_settings);

        SettingFieldAdapter cardAdapter = new SettingFieldAdapter(getApplicationContext(), jdata);

        recyclerView.setAdapter(cardAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        ImageButton back_btn = findViewById(R.id.back_btn_2);
        back_btn.setOnClickListener(v -> onBackPressed());
    }
}
