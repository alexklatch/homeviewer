package com.example.homeviewer.SettingsFiles;

public class SettingsTypes {

    public static final int INFORMATION = 0;
    public static final int DEVS = 1;
    public static final int ADDRESSING = 2;
    public static final int NETWORK = 3;
    public static final int MAIN = 4;
    public static final int ADD = 5;

}
