package com.example.homeviewer.SipFiles;

import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.homeviewer.R;
import com.example.homeviewer.Utils.EventBus.Event;
import com.example.homeviewer.Utils.EventBus.EventBus;
import com.example.homeviewer.Utils.EventBus.Events;
import com.example.homeviewer.Utils.ParseConfig;
import com.example.homeviewer.Utils.Utils;
import com.example.homeviewer.VideoCaptureFiles.VideoPlayerFragment;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;

public class CallActivity extends AppCompatActivity {

    private ImageButton ansButton;
    private ImageButton decButton;
    private SeekBar audSeek;

    private final String TAG = "HomeViewer.CallActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        ansButton = findViewById(R.id.ans_call);
        decButton = findViewById(R.id.dec_call);

        audSeek = findViewById(R.id.audSeek);

        ParseConfig parseConfig = new ParseConfig(this.getApplicationContext());

        HashMap<String, String> devs = null;
        HashMap<String, String> cams = null;

        try {
            parseConfig.parseFromLocal(Utils.getPrefs(this.getApplicationContext(), "FilePath"));
            devs = parseConfig.getDevs();
            cams = parseConfig.getRtpLinks();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        HashMap<String, String> finalDevs = devs;
        HashMap<String, String> finalCams = cams;

        EventBus.subscribe(msg -> {
            Event event = (Event) msg;
            int event_id = event.getEventId();
            switch (event_id) {
                case Events.ID_ANSWERED_CALL:
                    String username = event.getUsername();
                    String cam_id = finalDevs.get(username);
                    Log.d(TAG, "Camera id  " + cam_id);
                    if (!cam_id.isEmpty()) {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.add(R.id.call_video_container, VideoPlayerFragment.newInstance(finalCams.get(cam_id), 1300, 800), "video");
                        fragmentTransaction.commit();
                    }
                    break;
            }

        });

        ansButton.setOnClickListener(v -> EventBus.post(new Event(Events.ID_ANSWER_CALL)));

        decButton.setOnClickListener(v -> EventBus.post(new Event(Events.ID_DECLINE_CALL)));

        audSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                EventBus.post(new Event(Events.ID_CHANGE_AUD, progress));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

        });
    }
}
