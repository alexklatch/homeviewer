package com.example.homeviewer.SipFiles;

import android.util.Log;

import com.example.homeviewer.Utils.EventBus.Event;
import com.example.homeviewer.Utils.EventBus.EventBus;
import com.example.homeviewer.Utils.EventBus.Events;

import org.pjsip.pjsua2.Account;
import org.pjsip.pjsua2.CallOpParam;
import org.pjsip.pjsua2.OnIncomingCallParam;
import org.pjsip.pjsua2.OnRegStateParam;
import org.pjsip.pjsua2.pjsip_status_code;

public class SipAccount extends Account {

    private static final String TAG = "HomeViewer.account";

    @Override
    public void onIncomingCall(OnIncomingCallParam prm) {
        super.onIncomingCall(prm);
        Log.d(TAG, String.valueOf(prm.getCallId()));
        SipCall call = new SipCall(this, prm.getCallId());
        CallOpParam param = new CallOpParam();
        param.setStatusCode(pjsip_status_code.PJSIP_SC_OK);
        String inName = "";

        try {
            String contact = call.getInfo().getRemoteContact();
            inName = contact.substring(contact.indexOf(':') + 1, contact.indexOf('@'));
            EventBus.post(new Event(Events.ID_INCOMING_CALL, inName));
            Log.d(TAG, inName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String finalInName = inName;
        EventBus.subscribe(msg -> {
            Event event = (Event) msg;
            int event_id = event.getEventId();
            switch (event_id) {
                case Events.ID_ANSWER_CALL:
                    call.answer(param);
                    EventBus.post(new Event(Events.ID_ANSWERED_CALL, finalInName));
                    break;
                case Events.ID_DECLINE_CALL:
                    call.hangup(param);
                    call.delete();
                    break;
            }
        });

    }

    @Override
    public void onRegState(OnRegStateParam prm) {
        super.onRegState(prm);
        Log.d(TAG, "Account state " + prm.getStatus() + " " + prm.getReason());
    }


}
