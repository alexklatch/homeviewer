package com.example.homeviewer.SipFiles;

import android.util.Log;

import com.example.homeviewer.Utils.EventBus.Event;
import com.example.homeviewer.Utils.EventBus.EventBus;
import com.example.homeviewer.Utils.EventBus.Events;

import org.pjsip.pjsua2.Account;
import org.pjsip.pjsua2.AudioMedia;
import org.pjsip.pjsua2.Call;
import org.pjsip.pjsua2.CallInfo;
import org.pjsip.pjsua2.Endpoint;
import org.pjsip.pjsua2.EpConfig;
import org.pjsip.pjsua2.MediaConfig;
import org.pjsip.pjsua2.OnCallMediaStateParam;
import org.pjsip.pjsua2.pjmedia_type;



public class SipCall extends Call {
    public SipCall(Account acc, int call_id) {
        super(acc, call_id);
    }

    private final String TAG = "HomeViewer.SipCall";

    @Override
    public void onCallMediaState(OnCallMediaStateParam prm) {
        super.onCallMediaState(prm);

        CallInfo ci = null;
        try {
            ci = this.getInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }

        AudioMedia dev = null;
        AudioMedia playDev = null;

        EventBus.subscribe(msg ->
        {
            Event event = (Event) msg;
            int event_id = event.getEventId();
            switch (event_id) {
                case Events.ID_CHANGE_AUD:
                    Endpoint.instance().audDevManager().setOutputVolume(event.getVal());
                    break;
            }
        });



        try {
            dev = Endpoint.instance().audDevManager().getCaptureDevMedia();
            playDev = Endpoint.instance().audDevManager().getPlaybackDevMedia();
        } catch (Exception e) {
            e.printStackTrace();
        }

        AudioMedia aum = AudioMedia.typecastFromMedia(this.getMedia(0));

        try {

            dev.startTransmit(aum);
            aum.startTransmit(playDev);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
