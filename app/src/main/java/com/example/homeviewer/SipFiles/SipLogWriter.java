package com.example.homeviewer.SipFiles;

import android.util.Log;

import org.pjsip.pjsua2.LogEntry;

public class SipLogWriter extends org.pjsip.pjsua2.LogWriter {

    private final String TAG = "PJSIP";

    @Override
    public void swigTakeOwnership() {
        super.swigTakeOwnership();
    }

    @Override
    public void write(LogEntry entry) {
        super.write(entry);
        System.out.println(entry.getMsg());
    }
}
