package com.example.homeviewer;

import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.homeviewer.SettingsFiles.SettingFragment;
import com.example.homeviewer.VideoCaptureFiles.VideoFrameFragment;
import com.example.homeviewer.Worktable.WorktableFragment;

public class SlideAdapter extends FragmentPagerAdapter {
    private final String TAG = "HomeViewer.SlideAdapter";

    private Fragment[] childFragments;

    public SlideAdapter(FragmentManager fm) {
        super(fm);

        childFragments = new Fragment[]{
                VideoFrameFragment.newInstance(),
                WorktableFragment.newInstance(),
                SettingFragment.newInstance()
        };

    }


    @Override
    public Fragment getItem(int position) {
        return childFragments[position];
    }

    @Override
    public int getCount() {
        return childFragments.length;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        super.destroyItem(container, position, object);
        Log.d(TAG, "Item destroyed " + position);
    }
}
