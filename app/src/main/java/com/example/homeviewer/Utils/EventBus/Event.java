package com.example.homeviewer.Utils.EventBus;

public class Event {

    private int eventId;
    private String username;
    private int val;

    public Event(int eventId, String username) {
        this.eventId = eventId;
        this.username = username;
    }

    public Event(int eventId) {
        this.eventId = eventId;
    }

    public Event(int eventId, int val) {
        this.eventId = eventId;
        this.val = val;
    }

    public int getEventId() {
        return eventId;
    }

    public String getUsername() {
        return username;
    }

    public int getVal() {
        return val;
    }
}
