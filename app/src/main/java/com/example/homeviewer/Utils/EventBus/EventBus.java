package com.example.homeviewer.Utils.EventBus;

import androidx.annotation.NonNull;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class EventBus {

    private static PublishSubject<Object> subject = PublishSubject.create();

    private EventBus() {
        subject.subscribeOn(Schedulers.io());
    }

    public static Disposable subscribe(@NonNull Consumer<Object> action) {
        return subject.subscribe(action);
    }

    public static void post(@NonNull Object message) {
        subject.onNext(message);
    }

    public static void complete() {
        subject.onComplete();
    }

}
