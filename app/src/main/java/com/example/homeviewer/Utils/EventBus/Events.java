package com.example.homeviewer.Utils.EventBus;

public class Events {

    public Events() {

    }

    public static final int ID_INCOMING_CALL = 0;
    public static final int ID_ANSWER_CALL = 1;
    public static final int ID_ANSWERED_CALL = 2;
    public static final int ID_DECLINE_CALL = 3;
    public static final int ID_CHANGE_AUD = 4;
    public static final int ID_CALL_DTMF = 5;
    public static final int ID_GET_CONFIG = 6;
    public static final int ID_CHANGE_CONFIG = 7;
    public static final int ID_CURRNET_TIME = 7;
    public static final int ID_CONFIG_LOADED = 8;
    public static final int ID_SETTING_CLICK = 9;

}
