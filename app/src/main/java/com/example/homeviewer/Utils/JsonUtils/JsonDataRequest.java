package com.example.homeviewer.Utils.JsonUtils;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.homeviewer.Utils.EventBus.Event;
import com.example.homeviewer.Utils.EventBus.EventBus;
import com.example.homeviewer.Utils.EventBus.Events;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonDataRequest {

    private String url = "http://192.168.31.30:8080/index/api/";
    private Context context;
    private RequestQueue requestQueue;
    private JSONObject db = null;
    private final String TAG = "HomeViewer.JsonParser";

    public JsonDataRequest(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
    }

    public JSONObject getDb() {
        return db;
    }


    public void reqConfig() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,

                response -> {
                    try {
                        JSONObject jsonObject = new JSONObject(response.toString());
                        Log.d(TAG, jsonObject.toString());
                        db = jsonObject;
                        EventBus.post(new Event(Events.ID_CONFIG_LOADED));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(TAG, e.toString());
                    }
                },
                error -> Log.e(TAG, error.toString())
        );
        requestQueue.add(request);
    }

}
