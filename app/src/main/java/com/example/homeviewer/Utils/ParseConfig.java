package com.example.homeviewer.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ParseConfig {

    private int path;
    private Context ctx;
    private Resources resources;
    private static final String TAG = "HomeViewer.ConfigParser";
    private static boolean isParsed = false;
    private static JSONObject wholeJsonObject;

    public ParseConfig(int path, Context ctx) {
        this.path = path;
        this.ctx = ctx;
        resources = ctx.getResources();
    }

    public ParseConfig(Context ctx) {
        this.ctx = ctx;
    }

    public boolean parseFromLocal(String path) throws IOException, JSONException {
        File file = new File(path);
        BufferedReader br = new BufferedReader(new FileReader(file));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();

        wholeJsonObject = new JSONObject(sb.toString());
        Log.d(TAG, "Parsing from file success!");
        isParsed = true;
        return true;
    }

    public String[] getCreds() throws JSONException {
        if (isParsed) {
            JSONObject obj = wholeJsonObject.getJSONObject("creds");
            String username = obj.getString("username");
            String pass = obj.getString("password");
            return new String[]{username, pass};
        }
        Log.e(TAG, "Please parse config file first!");
        return new String[0];
    }

    public boolean parse() throws IOException, JSONException {
        InputStream config = resources.openRawResource(path);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(config, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (Exception e) {
            return false;
        } finally {
            config.close();
        }

        wholeJsonObject = new JSONObject(writer.toString());
        Log.d(TAG, "Parsing success!");

        isParsed = true;

        File dir = new File(ctx.getFilesDir(), "config");
        if (!dir.exists()) {
            dir.mkdir();
        }

        Log.d(TAG, dir.getAbsolutePath());

        File conf = new File(dir, "config.json");
        FileWriter w = new FileWriter(conf);
        w.append(wholeJsonObject.toString());
        w.flush();
        w.close();

        Utils.addToPrefs(ctx, "FilePath", conf.getAbsolutePath());

        return true;
    }

    public int getConfigCount(String name) throws JSONException {
        if (isParsed) {
            JSONObject obj = wholeJsonObject.getJSONObject(name);
            int count = obj.getInt("count");
            return count;
        }
        Log.e(TAG, "Please parse config file first!");
        return 0;
    }

    public List<String> getRtpNames() throws JSONException {
        List<String> names = new ArrayList<>();

        if (isParsed) {
            JSONObject rtpLinks = wholeJsonObject.getJSONObject("rtp_Links");
            int linkCount = rtpLinks.getInt("count");
            Log.d(TAG, String.valueOf(linkCount));
            for (int i = 0; i < linkCount; i++) {
                String name = "name_" + i;
                Log.d(TAG, rtpLinks.getString(name));
                names.add(rtpLinks.getString(name));
            }
        }
        return names;
    }

    public HashMap<String, String> getRtpLinks() throws JSONException {
        if (isParsed) {
            HashMap<String, String> rtpList = new HashMap<>();
            JSONObject rtpLinks = wholeJsonObject.getJSONObject("rtp_Links");
            int linkCount = rtpLinks.getInt("count");
            for (int i = 0; i < linkCount; i++) {
                String linkName = "link_" + i;
                rtpList.put(linkName, rtpLinks.getString(linkName));
            }
            Log.d(TAG, String.valueOf(rtpList.size()));
            return rtpList;
        } else {
            Log.d(TAG, "Not parsed");
            return null;
        }
    }

    public static HashMap<String, String> getDevs() throws JSONException {
        if (isParsed) {
            HashMap<String, String> devList = new HashMap<>();
            JSONObject devs = wholeJsonObject.getJSONObject("devices");
            int devCount = devs.getInt("count");
            for (int i = 0; i < devCount; i++) {
                String camName = "cam_" + i;
                String contName = "contact_" + i;
                devList.put(devs.getString(contName), devs.getString(camName));
            }
            Log.d(TAG, String.valueOf(devList.size()));
            return devList;
        } else {
            Log.d(TAG, "Not parsed");
            return null;
        }
    }

    public ArrayList<String> getSipLink() throws JSONException {

        if (isParsed) {
            ArrayList<String> sipLink = new ArrayList<>();

            JSONObject link = wholeJsonObject.getJSONObject("sip_link");
            sipLink.add(link.getString("username"));
            sipLink.add(link.getString("password"));
            sipLink.add(link.getString("ip"));
            sipLink.add(link.getString("port"));

            return sipLink;
        } else {
            Log.d(TAG, "Not parsed");
            return null;
        }
    }

    public List<String> getSipContacts() throws JSONException {
        List<String> contacts = new ArrayList<>();

        if (isParsed) {
            JSONObject sip_contacts = wholeJsonObject.getJSONObject("sip_contacts");
            int linkCount = sip_contacts.getInt("count");
            Log.d(TAG, String.valueOf(linkCount));
            for (int i = 0; i < linkCount; i++) {
                String name = "contact_" + i;
                Log.d(TAG, sip_contacts.getString(name));
                contacts.add(sip_contacts.getString(name));
            }
        }
        return contacts;
    }

    public void changeConfig(String subParamName, String paramName, String paramValue) throws JSONException {
        if (isParsed) {
            wholeJsonObject.getJSONObject(subParamName).put(paramName, paramValue);
            File conf = new File(Utils.getPrefs(ctx.getApplicationContext(), "FilePath"));

            try (FileWriter file = new FileWriter(conf)) {

                file.write(wholeJsonObject.toString());
                file.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void changeConfig(String subParamName, String paramName, int paramValue) throws JSONException {
        if (isParsed) {
            wholeJsonObject.getJSONObject(subParamName).put(paramName, paramValue);
            File conf = new File(Utils.getPrefs(ctx.getApplicationContext(), "FilePath"));

            try (FileWriter file = new FileWriter(conf)) {

                file.write(wholeJsonObject.toString());
                file.flush();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

}
