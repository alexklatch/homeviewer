package com.example.homeviewer.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Utils {

    public static void addToPrefs(Context context, String key, String val) {
        SharedPreferences sharedPref = context.getApplicationContext().getSharedPreferences("Prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.commit();
    }

    public static String getPrefs(Context context, String key) {
        SharedPreferences sharedPref = context.getSharedPreferences("Prefs", Context.MODE_PRIVATE);
        return sharedPref.getString(key, null);
    }


}
