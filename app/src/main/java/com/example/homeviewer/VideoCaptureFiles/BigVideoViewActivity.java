package com.example.homeviewer.VideoCaptureFiles;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.homeviewer.R;

import static com.example.homeviewer.VideoCaptureFiles.VideoPlayerFragment.isImmersiveAvailable;

public class BigVideoViewActivity extends AppCompatActivity {

    private final String TAG = "HomeViewer.BigVideoViewActivity";

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_big_video_view);
        Log.d(TAG, "Started");
        FrameLayout frameLayout = findViewById(R.id.big_video_container);
        frameLayout.setOnClickListener(this::onClick);

        setFullscreen(this);

        Intent intent = getIntent();
        String url = intent.getStringExtra("link");

        String cUrl = url.split("subtype=")[0];
        Log.d(TAG, cUrl + "subtype=0");
        url = cUrl + "subtype=0";

        Log.d(TAG, url);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.big_video_container, VideoPlayerFragment.newInstance(url, 1300, 800, false), "video");
        fragmentTransaction.commit();
    }

    public void onClick(View v) {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    public void setFullscreen(Activity activity) {
        if (Build.VERSION.SDK_INT > 10) {
            int flags = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_FULLSCREEN;

            if (isImmersiveAvailable()) {
                flags |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            }

            activity.getWindow().getDecorView().setSystemUiVisibility(flags);
        } else {
            activity.getWindow()
                    .setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

}
