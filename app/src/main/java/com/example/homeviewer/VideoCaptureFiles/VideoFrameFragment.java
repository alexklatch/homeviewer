package com.example.homeviewer.VideoCaptureFiles;

import android.annotation.SuppressLint;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import androidx.annotation.LongDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.homeviewer.R;
import com.example.homeviewer.Utils.ParseConfig;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class VideoFrameFragment extends Fragment {

    private final String TAG = "HomeViewer.VideoFrameFragment";
    private ParseConfig parseConfig;

    public static VideoFrameFragment newInstance() {
        VideoFrameFragment f = new VideoFrameFragment();
        return f;
    }

    private int[] vFragmentsId = new int[]{
            R.id.video_container0,
            R.id.video_container1,
            R.id.video_container2,
            R.id.video_container3
    };

    @SuppressLint("LongLogTag")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_main, null);
        HashMap<String, String> rtpLinks = null;

        parseConfig = new ParseConfig(R.raw.config, getActivity().getApplicationContext());

        boolean configParseStatus = false;
        try {
            configParseStatus = parseConfig.parse();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (configParseStatus) {
            try {
                rtpLinks = parseConfig.getRtpLinks();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            FragmentManager fragmentManager = getChildFragmentManager();

            for (int i = 0; i < rtpLinks.size() - 1; i++) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                String url = rtpLinks.get("link_" + i);
                Log.d(TAG, "Vid url " + url);
                FrameLayout vid_container = v.findViewById(vFragmentsId[i]);
                fragmentTransaction.add(vFragmentsId[i], VideoPlayerFragment.newInstance(url, 590, 390), "frag " + i);
                fragmentTransaction.commit();
            }
        } else {
            Log.d(TAG, "Config not parsed");
        }

        return v;
    }

    private List<FrameLayout> layouts = new ArrayList<>();

    private void createViewTable(int num, FrameLayout root) {
        for (int i = 0; i < num; i++) {
            if (i % 2 == 0) {

            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
