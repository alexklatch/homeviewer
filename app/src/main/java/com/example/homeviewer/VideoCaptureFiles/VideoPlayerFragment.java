package com.example.homeviewer.VideoCaptureFiles;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.homeviewer.R;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.util.ArrayList;


public class VideoPlayerFragment extends Fragment implements IVLCVout.Callback {

    private final String TAG = "HomeViewer.Video";

    private SurfaceView vlcVideoLayout;
    private LibVLC libvlc;
    private MediaPlayer mMediaPlayer;
    private Context context;
    private SurfaceHolder holder;
    private boolean isClickable = true;
    private int mVideoWidth;
    private int mVideoHeight;
    private ViewGroup mContainer;

    public String url = "";

    public static VideoPlayerFragment newInstance(String url, int w, int h) {
        VideoPlayerFragment f = new VideoPlayerFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString("url", url);
        args.putInt("w", w);
        args.putInt("h", h);
        f.setArguments(args);
        return f;
    }


    public static VideoPlayerFragment newInstance(String url, int w, int h, boolean isClickable) {
        VideoPlayerFragment f = new VideoPlayerFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString("url", url);
        args.putInt("w", w);
        args.putInt("h", h);
        args.putBoolean("clk", isClickable);
        f.setArguments(args);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "Created View");
        //context = container.getContext();
        Bundle args = getArguments();
        if (args.containsKey("clk")) {
            isClickable = args.getBoolean("clk");
        }

        mVideoWidth = args.getInt("w");
        mVideoHeight = args.getInt("h");

        url = args.getString("url");

        View view = inflater.inflate(R.layout.fragment_video_player, container, false);
        mContainer = container;

        context = getActivity();
        vlcVideoLayout = view.findViewById(R.id.fragment_videoview_surface);
        holder = vlcVideoLayout.getHolder();
        holder.addCallback(mCallback);

        return inflater.inflate(R.layout.fragment_video_player, container, false);
    }

    SurfaceHolder.Callback mCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder surfaceHolder) {

        }

        @Override
        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
            Log.d(TAG, i + " " + i1 + i2);
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

        }
    };

    private boolean status = false;

    public View.OnClickListener viewClickCallback = view -> {
        Log.d(TAG, "Pressed " + vlcVideoLayout.getId());

        if (!status) {
            status = true;
//            setFullscreen(getActivity());
            Intent intent = new Intent(context, BigVideoViewActivity.class);
            intent.putExtra("link", url);
            startActivityForResult(intent, 1);

        } else {
            status = false;
            //          exitFullscreen(getActivity());
        }

    };

    @Override
    public void onStart() {
        super.onStart();
        String rtspUrl = url;
        Log.d(TAG, "Playing back " + rtspUrl);

        if (isClickable) {
            vlcVideoLayout.setOnClickListener(viewClickCallback);
        }

        IVLCVout vout = mMediaPlayer.getVLCVout();
        vout.setVideoView(vlcVideoLayout);
        // vout.setWindowSize(mVideoWidth, mVideoHeight);
        mMediaPlayer.setAspectRatio("16:10");
        vout.addCallback(this);
        vout.attachViews();

        final Media media = new Media(libvlc, Uri.parse(rtspUrl));

        mMediaPlayer.setMedia(media);

        media.release();

        mMediaPlayer.play();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        vlcVideoLayout = view.findViewById(R.id.fragment_videoview_surface);

        ArrayList<String> options = new ArrayList<>();
        options.add("-vvv"); // verbosity
        options.add("--no-audio");

        libvlc = new LibVLC(this.getContext(), options);

        mMediaPlayer = new MediaPlayer(libvlc);
        //mMediaPlayer.getVLCVout().setWindowSize(470, 400);
    }

    @Override
    public void onStop() {
        super.onStop();
        mMediaPlayer.stop();
        mMediaPlayer.getVLCVout().detachViews();
        mMediaPlayer.getVLCVout().removeCallback(this);
    }

    @Override
    public void onSurfacesCreated(IVLCVout vlcVout) {

    }

    @Override
    public void onSurfacesDestroyed(IVLCVout vlcVout) {

    }

    public static boolean isImmersiveAvailable() {
        return android.os.Build.VERSION.SDK_INT >= 19;
    }

}
