package com.example.homeviewer.Worktable;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.homeviewer.R;
import com.example.homeviewer.Utils.EventBus.Event;
import com.example.homeviewer.Utils.EventBus.EventBus;
import com.example.homeviewer.Utils.EventBus.Events;


public class WorktableFragment extends Fragment {

    private final String TAG = "HomeViewer.WorktableFragment";
    private int timer = 0;
    private String cur_time = "0:00";
    private int[] start_time = {9, 48};
    private Handler handler;

    public static WorktableFragment newInstance() {
        WorktableFragment f = new WorktableFragment();
        return f;
    }

    private TextView clock;

    private void updateClock(String text) {
        clock.setText(text);
    }

    @SuppressLint("LongLogTag")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.workspace_layout, null);
        clock = v.findViewById(R.id.clock_txt);

        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                Log.d(TAG, "In handler");
                if (msg.arg1 == 0) {
                    Log.d(TAG, msg.obj + "");
                    clock.setText((String) msg.obj);
                }
            }
        };

        EventBus.subscribe(msg -> {
            Event event = (Event) msg;
            int event_id = event.getEventId();
            String result = event.getUsername();

            switch (event_id) {
                case Events.ID_CURRNET_TIME:
                    Log.d(TAG, "Time has come");
                    Log.d(TAG, "Time is " + result);
                    Message time_msg = handler.obtainMessage(0, result);
                    time_msg.sendToTarget();
                    break;
            }
        });


        return v;
    }
}
